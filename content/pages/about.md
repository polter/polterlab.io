Title: About Me
Slug: about

Hi there. I’m Pavel Artsishevskii, known in online as `polter` or `polter-rnd`,
and this is a blog where I post about different technical topics I'm interested in.

I'm a software engineer from Rostov-on-Don/Russia, now living in Turkey.
I graduated with master degree in computer science from the
[Southern Federal University](https://sfedu.ru/index_eng.php){:target="_blank"} in 2015,
and am currently employed in [CloudLinux](http://cloudlinux.com){:target="_blank"},
working as a C/C++ engineer and Team Leader on userspace live-patching services for Linux in
[TuxCare](https://tuxcare.com/){:target="_blank"} department.
It requires specific skills such as working with raw ELF, in-detail knowledge of Linux userspace,
various in-memory tricks and *a lot* of debugging.

Prior to this, I spent three years working at [CVisionLab](http://cvisionlab.com){:target="_blank"},
developing software connected with pattern recognition algorithms and other computer vision stuff.
Designed and implemented various deep learning models as well as classic mathematical algorithms.

For more details about my work experiece please visit my [CV](/cv/){:target="_blank"}.

I have expert knowledge in Linux userspace, C/C++, Python and Bash
as well as considerable experience of working with UI frameworks like
[GTK+](https://gtk.org){:target="_blank"},
[Qt](https://contribute.qt-project.org){:target="_blank"},
[FLTK](https://fltk.org){:target="_blank"}.

I'm passionate about [Open Source](https://opensource.org){:target="_blank"} and always look for
opportunities to develop my skills and contribute on the Software front.
My contributions include enhancements and bug fixes to various famous projects,
one can find my commits in [Qt](https://codereview.qt-project.org){:target="_blank"},
[GIMP](https://gitlab.gnome.org/GNOME/gimp){:target="_blank"},
[ProFTPD](https://github.com/proftpd/proftpd){:target="_blank"},
[libdecor](https://gitlab.freedesktop.org/libdecor/libdecor){:target="_blank"} and others.

I'm brilliant problem solver and my passion is to use my algorithmic and debugging skills
to make a product better for its users.

When I have a free time, I like to do my hobby projects
(see my [GitHub](https://github.com/polter-rnd){:target="_blank"}), playing table tennis,
travelling by bycicle in summer and skiing in winter.

I'm always interested in meeting new opportunities and positive people
and love to explore new technologies.
