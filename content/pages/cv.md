Title: Curriculum Vitae
Slug: cv
Summary: I am a passionate Software Engineer and a Team Lead with more than 10 years
         experience in system programming, backend and end-user software development.
         I have expert knowledge in Linux userspace, C/C++, Python and Bash.
         Also I have been coding on Go and Java for a while and have considerable
         experience with UI frameworks like GTK+ and Qt.

<span class="icon fa fa-file"></span> Download it as PDF: [click here](/files/pavel_artsishevskii.pdf)

<iframe id="cv" src="/pdfjs/web/viewer.html?file=/files/pavel_artsishevskii.pdf#pagemode=none"
    style="width: 100%; border: 1px solid #ebebeb"></iframe>
<script type="text/javascript">
window.onload = window.onresize = function() {
    var iframe = document.getElementById("cv");
    var iframeDoc = iframe.contentWindow.document;

    // Maintain A4 aspect ratio
    iframe.style.height = iframeDoc.body.scrollWidth * 1.3 + 'px';

    // Allow viewerport to shrink
    var mainContainer = iframeDoc.getElementById('mainContainer');
    mainContainer.style.minWidth = 0;

    // Remove unneeded controls
    ['sidebarToggle', 'editorModeButtons'].forEach(name => {
        node = iframeDoc.getElementById(name);
        if (node)
            node.parentNode.removeChild(node);
    });
}
</script>
