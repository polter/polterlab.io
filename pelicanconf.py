# Theme-specific settings
SITENAME = 'Pavel A. blog'
DOMAIN = 'polter.gitlab.io'
BIO_TEXT = \
    '<ul dir="rtl">' \
        '<li>C/C++/Linux developer</li>' \
        '<li>Open-Source enthusiast</li>' \
        '<li>Fedora and GNOME fan</li>' \
    '</ul>' \
    '<p>Employed in <a href="https://mbition.io" target="_blank">MBition&nbsp;GmbH</a></p>' \
    '<hr>'
FOOTER_TEXT = \
    'Powered by <a href="http://getpelican.com">Pelican</a> ' \
    'and <a href="http://pages.gitlab.io">GitLab&nbsp;Pages</a>.'

SITE_AUTHOR = 'Pavel Artsishevskii'
INDEX_DESCRIPTION = 'Blog about Linux tips & tricks, software development ' \
                    'and various technical stuff'

SIDEBAR_LINKS = [
    '<a href="/about/">About Me</a>',
    '<a href="/cv/">CV</a>',
    '<a href="/archive/">Archive</a>',
]

ICONS_PATH = 'images/icons'

SOCIAL_ICONS = [
    ('https://github.com/polter-rnd', 'GitHub', 'fa-github'),
    ('https://gitlab.com/polter', 'GitLab', 'fa-gitlab'),
    ('https://linkedin.com/in/polter', 'LinkedIn', 'fa-linkedin'),
    ('https://instagram.com/polter_rnd', 'Instagram', 'fa-instagram'),
    ('/atom.xml', 'Atom Feed', 'fa-feed'),
]

THEME_COLOR = '#222222'

GOOGLE_FONTS = [
    'Noto Serif',
    'Inconsolata'
]

# Pelican settings
RELATIVE_URLS = True
SITEURL = ''
TIMEZONE = 'Europe/Moscow'
DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%-d %B, %Y'
DEFAULT_PAGINATION = False
SUMMARY_MAX_LENGTH = 42

THEME = 'theme/pneumatic'

ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}/'
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'

ARCHIVES_SAVE_AS = 'archive/index.html'
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

# Disable authors, categories, tags, and category pages
DIRECT_TEMPLATES = ['index', 'archives']
CATEGORY_SAVE_AS = ''

# Disable Atom feed generation
FEED_ATOM = 'atom.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

TYPOGRIFY = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.admonition': {},
        'markdown.extensions.codehilite': {'linenums': None},
        'markdown.extensions.extra': {},
    },
    'output_format': 'html5',
}

CACHE_CONTENT = False
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_PATH = 'develop'

PATH = 'content'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['posts']

templates = ['404.html']
TEMPLATE_PAGES = {page: page for page in templates}

STATIC_PATHS = ['images', 'files', 'extra', 'pdfjs']
IGNORE_FILES = ['.DS_Store', 'pneumatic.scss', 'pygments.css']

extras = ['favicon.ico', 'robots.txt']
EXTRA_PATH_METADATA = {'extra/%s' % file: {'path': file} for file in extras}

PLUGINS = ['webassets', 'neighbors', 'sitemap']
WEBASSET_SOURCE_PATHS = ['static']
WEBASSET_CONFIG = [
    ('cache', False),
    ('manifest', False),
    ('url_expire', False),
    ('versions', False),
]
SITEMAP = {
    'format': 'xml',
    'exclude': ['404.html']
}
